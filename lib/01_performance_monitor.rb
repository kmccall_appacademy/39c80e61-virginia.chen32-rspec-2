require 'time'
def measure(n=0)
  start=Time.now
  if n==0
    yield
    return Time.now-start
  else n.times {|el| result = yield(el)}
    return (Time.now-start)/n
  end
end
