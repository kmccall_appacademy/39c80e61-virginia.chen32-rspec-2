def reverser
  reverse = yield.split(" ").each do |word|
    word.reverse!
  end
  reverse.join(" ")
end

def adder(int=1)
  yield+int
end

def repeater(n=1)
  counter = 0
  while counter < n
    yield
    counter+=1
  end
end
